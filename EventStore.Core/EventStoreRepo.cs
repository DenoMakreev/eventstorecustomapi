﻿using EventStore.ClientAPI;
using EventStoreInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EventStore.Core
{
    public class EventStoreRepo : IEventStoreRepo
    {
        private IEventStoreConnection _connection;

        public EventStoreRepo()
        {

        }

        public string CreateStreamName(string typeName, Guid id)
        {
            string streamName = $"{typeName}-{id.ToString()}";

            return streamName;
        }

        //Hardcoded to connect to localhost
        public void CreateConnection()
        {
            IPAddress localhost = IPAddress.Loopback;
            int port = 1113;
            IPEndPoint endpoint = new IPEndPoint(localhost, port);

            _connection = EventStoreConnection.Create(endpoint);
            _connection.ConnectAsync().Wait();

            Console.WriteLine($"Connected to EventStore on Localhost on port 1113");
        }

        public void WriteToStream(string streamName, object eventToAppend)
        {
            var jsonString = JsonConvert.SerializeObject(eventToAppend, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None });
            var jsonPayload = Encoding.UTF8.GetBytes(jsonString);
            var eventStoreDataType = new EventData(Guid.NewGuid(), eventToAppend.GetType().Name, true, jsonPayload, null);

            _connection.AppendToStreamAsync(streamName, ExpectedVersion.Any, eventStoreDataType);
        }


        public void WriteToStreamWithVersion(string streamName, object eventToAppend, long eventVersion)
        {
            var jsonString = JsonConvert.SerializeObject(eventToAppend, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None });
            var jsonPayload = Encoding.UTF8.GetBytes(jsonString);
            var eventStoreDataType = new EventData(
                Guid.NewGuid(),
                "eventType",
                true,
                Encoding.ASCII.GetBytes("{'somedata' : " + eventVersion + "}"),
                Encoding.ASCII.GetBytes("{'metadata' : " + eventVersion + "}")
                );

            _connection.AppendToStreamAsync(streamName, eventVersion, eventStoreDataType);
        }

        public void SetMetaData(string streamName, long truncBefore)
        {
            try
            {
                var rdata = StreamMetadata.Create(null, null, truncBefore, null);

                _connection.SetStreamMetadataAsync(streamName, -2, rdata).Wait();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Disconnect()
        {
            _connection.Dispose();
        }
    }
}
