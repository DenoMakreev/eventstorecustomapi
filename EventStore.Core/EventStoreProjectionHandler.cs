﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.Common.Log;
using EventStore.ClientAPI.Projections;
using EventStore.ClientAPI.SystemData;
using EventStoreInterfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace EventStore.Core
{
    public class EventStoreProjectionHandler : IEventStoreProjectionsHandler
    {
        private IEventStoreConnection _connection;
        private ProjectionsManager _projectionsManager;
        private IPEndPoint _endpoint;

        public EventStoreProjectionHandler()
        {
            CreateProjectionsManager();
        }

        public string CreateStreamName(string typeName, Guid id)
        {
            string streamName = $"{typeName}-{id.ToString()}";

            return streamName;
        }

        //Hardcoded to connect to localhost
        public void CreateConnection()
        {
            IPAddress localhost = IPAddress.Loopback;
            int port = 1113;
            IPEndPoint endpoint = new IPEndPoint(localhost, port);
            _endpoint = endpoint;

            _connection = EventStoreConnection.Create(endpoint);
            _connection.ConnectAsync().Wait();

            Console.WriteLine($"Connected to EventStore on Localhost on port 1113");
        }

        private void CreateProjectionsManager()
        {
            _projectionsManager = new ProjectionsManager(new ConsoleLogger(), _endpoint, TimeSpan.FromSeconds(5));
        }

        public void EnableProjection(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.EnableAsync(projectionName, user);
        }

        public void DisableProjection(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.DisableAsync(projectionName, user);
        }

        public void AbortProjection(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.AbortAsync(projectionName, user);
        }

        public void DeleteProjection(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.DeleteAsync(projectionName, user);
        }

        public void CreateOneTimeProjection(string query, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.CreateOneTimeAsync(query, user);
        }

        public void CreateContinuousProjection(string projectionName, string query, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            _projectionsManager.CreateContinuousAsync(projectionName, query, user);
        }

        public async Task<string> GetContinuousProjectionState(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionState = await _projectionsManager.GetStateAsync(projectionName, user);

            return projectionState;
        }

        public Task<string> GetContinuousProjectionResult(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionResult = _projectionsManager.GetResultAsync(projectionName, user);

            return projectionResult;
        }


        public Task<string> GetContinuousProjectionStatistics(string projectionName, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionStatisticsResult = _projectionsManager.GetStatisticsAsync(projectionName, user);

            return projectionStatisticsResult;
        }

        public Task<List<ProjectionDetails>> ListAllProjections(string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionResult = _projectionsManager.ListAllAsync(user);

            return projectionResult;
        }

        public Task<List<ProjectionDetails>> ListAllOneTimeProjections(string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionResult = _projectionsManager.ListOneTimeAsync(user);

            return projectionResult;
        }

        public Task<List<ProjectionDetails>> ListAllContinuousProjections(string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var projectionResult = _projectionsManager.ListContinuousAsync(user);

            return projectionResult;
        }
    }
}
