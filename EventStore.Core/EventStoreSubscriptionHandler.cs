﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using EventStoreInterfaces;
using System;
using System.Net;
using System.ServiceModel.Security;

namespace EventStore.Core
{
    public class EventStoreSubscriptionHandler : IEventStoreSubscriptionHandler
    {
        private IEventStoreConnection _connection;

        private EventStorePersistentSubscriptionBase _persistentSubscription;

        public EventStoreSubscriptionHandler()
        {

        }

        public string CreateStreamName(string typeName, Guid id)
        {
            string streamName = $"{typeName}-{id.ToString()}";

            return streamName;
        }

        //Hardcoded to connect to localhost
        public void CreateConnection()
        {
            IPAddress localhost = IPAddress.Loopback;
            int port = 1113;
            IPEndPoint endpoint = new IPEndPoint(localhost, port);

            _connection = EventStoreConnection.Create(endpoint);
            _connection.ConnectAsync().Wait();

            Console.WriteLine($"Connected to EventStore on Localhost on port 1113");
        }

        public void SubscribeToStream(string streamName, Action<EventStoreCatchUpSubscription, ResolvedEvent> handleEventAppeeared)
        {
            _connection.SubscribeToStreamFrom(streamName, 0, CatchUpSubscriptionSettings.Default, handleEventAppeeared);
        }

        public void CreatePersistentSubscription(string streamName, string groupName, UserCredentials user)
        {
            PersistentSubscriptionSettings settings = PersistentSubscriptionSettings.Create().DoNotResolveLinkTos().StartFromCurrent();

            try
            {
                _connection.CreatePersistentSubscriptionAsync(streamName, groupName, settings, user).Wait();
            }

            catch (AggregateException ex)
            {
                if (ex.InnerException.GetType() != typeof(InvalidOperationException)
                    && ex.InnerException?.Message != $"Subscription group {groupName} on stream {streamName} already exists")
                {
                    throw;
                }
            }
        }

        public void CreatePersistentSubscriptionWithDefinedSettings(string streamName, string groupName, string userName, string password, int checkpointCount = 10, int maxRetryCount = 10)
        {
            var user = new UserCredentials(userName, password);

            PersistentSubscriptionSettings settings = PersistentSubscriptionSettings.Create()
                .DoNotResolveLinkTos()
                .StartFromCurrent()
                .MinimumCheckPointCountOf(checkpointCount)
                .WithMaxRetriesOf(maxRetryCount);

            try
            {
                _connection.CreatePersistentSubscriptionAsync(streamName, groupName, settings, user).Wait();
            }

            catch (AggregateException ex)
            {
                if (ex.InnerException.GetType() != typeof(InvalidOperationException)
                    && ex.InnerException?.Message != $"Subscription group {groupName} on stream {streamName} already exists")
                {
                    throw;
                }
            }
        }

        public void ConnectToPersistentSubscription(string streamName, string groupName, Action<EventStorePersistentSubscriptionBase, ResolvedEvent> handleEventAppeared, 
            Action<EventStorePersistentSubscriptionBase, SubscriptionDropReason, Exception> subDropped, string userName, string password)
        {
            var user = new UserCredentials(userName, password);

            var bufferSize = 10;
            var autoAck = true;

            _persistentSubscription = _connection.ConnectToPersistentSubscription(streamName, groupName, handleEventAppeared, subDropped, user, bufferSize, autoAck);
        }

        public void Disconnect()
        {
            _connection.Dispose();
        }
    }
}
