It's a small custom API that uses EventStoreClientAPI to connect to EventStore.
It has functionality to Write Events and create catchup and persistent subscriptions.

For an example on how to use it check CqrsSingle that can be downloaded from 
https://gitlab.com/DenoMakreev/cqrssinlgle