﻿using EventStore.ClientAPI.Projections;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStoreInterfaces
{
    public interface IEventStoreProjectionsHandler
    {
        void CreateConnection();
        string CreateStreamName(string typeName, Guid id);
        void EnableProjection(string projectionName, string userName, string password);
        void DisableProjection(string projectionName, string userName, string password);
        void AbortProjection(string projectionName, string userName, string password);
        void DeleteProjection(string projectionName, string userName, string password);
        void CreateOneTimeProjection(string query, string userName, string password);
        void CreateContinuousProjection(string projectionName, string query, string userName, string password);
        Task<string> GetContinuousProjectionState(string projectionName, string userName, string password);
        Task<string> GetContinuousProjectionResult(string projectionName, string userName, string password);
        Task<string> GetContinuousProjectionStatistics(string projectionName, string userName, string password);
        Task<List<ProjectionDetails>> ListAllProjections(string userName, string password);
        Task<List<ProjectionDetails>> ListAllOneTimeProjections(string userName, string password);
        Task<List<ProjectionDetails>> ListAllContinuousProjections(string userName, string password);

    }
}
