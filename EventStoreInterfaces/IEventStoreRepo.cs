﻿using System;

namespace EventStoreInterfaces
{
    public interface IEventStoreRepo
    {
        void CreateConnection();
        string CreateStreamName(string typeName, Guid id);
        void WriteToStream(string streamName, object eventToAppend);
        void Disconnect();
    }
}
