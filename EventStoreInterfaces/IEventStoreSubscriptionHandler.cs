﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using System;

namespace EventStoreInterfaces
{
    public interface IEventStoreSubscriptionHandler
    {
        void CreateConnection();
        string CreateStreamName(string typeName, Guid id);
        void SubscribeToStream(string streamName, Action<EventStoreCatchUpSubscription, ResolvedEvent> handleEventAppeeared);
        void CreatePersistentSubscription(string streamName, string groupName, UserCredentials user);
        void CreatePersistentSubscriptionWithDefinedSettings(string streamName, string groupName, string userName, string password, int checkpointCount, int maxRetryCount);
        void ConnectToPersistentSubscription(string streamName, string groupName, Action<EventStorePersistentSubscriptionBase, ResolvedEvent> handleEventAppeared,
            Action<EventStorePersistentSubscriptionBase, SubscriptionDropReason, Exception> subDropped, string userName, string password);
        void Disconnect();
    }
}
